#!/bin/bash

CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
INBOUNDNAME="cfgcheckerinb"
DEPLOYNAME="cfgcheckerdep"
DOMAIN="cfgcheckerdomain"
SERVICEURL="cfgchecker-${CLUSTERNAME}.${REFERENCEDOMAIN}"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

KAM_CMD="kam"
KUMORICTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
	${KAM_CMD} mod dependency --delete kumori.systems/kumori
	${KAM_CMD} mod dependency kumori.systems/kumori/@1.0.6
  ${KAM_CMD} mod relink
  cd ..
  ;;

'create-domain')
  ${KUMORICTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'deploy-inbound')
  ${KUMORICTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CLUSTERCERT
  ;;

'deploy-service')
  ${KUMORICTL_CMD} register deployment $DEPLOYNAME \
    --deployment ./manifests/deployments/hello1 \
    --comment "Config checker service" \
    --wait 5m
  ;;

'dry-run')
  time ${KUMORICTL_CMD} register deployment $DEPLOYNAME \
    --deployment ./manifests/deployments/hello1 \
    --wait 5m --dry-run --keep-tmp-files
  ;;

'link')
  ${KUMORICTL_CMD} link $DEPLOYNAME:service $INBOUNDNAME:inbound
  ;;

'deploy-all')
  ${KUMORICTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ${KUMORICTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CLUSTERCERT
  ${KUMORICTL_CMD} register deployment $DEPLOYNAME \
    --deployment ./manifests/deployments/hello1 \
    --comment "Config checker service" \
    --wait 5m
  ${KUMORICTL_CMD} link $DEPLOYNAME:service $INBOUNDNAME:inbound
  ;;

'update-service-1')
  ${KUMORICTL_CMD} update deployment $DEPLOYNAME -y \
    --deployment ./manifests/deployments/hello1 \
    --comment "Update to VSET 1" \
    --wait 5m
  ;;

'update-service-2')
  ${KUMORICTL_CMD} update deployment $DEPLOYNAME -y \
    --deployment ./manifests/deployments/hello2 \
    --comment "Update to VSET 2" \
    --wait 5m
  ;;

'update-service-3')
  ${KUMORICTL_CMD} update deployment $DEPLOYNAME -y \
    --deployment ./manifests/deployments/hello3 \
    --comment "Update to VSET 3" \
    --wait 5m
  ;;

'describe')
  ${KUMORICTL_CMD} describe deployment $DEPLOYNAME
  ;;

# Test
'test-data')
  curl https://${SERVICEURL}/data
  ;;

'test-deployment')
  curl https://${SERVICEURL}/deployment
  ;;

'unlink')
  ${KUMORICTL_CMD} unlink $DEPLOYNAME:service $INBOUNDNAME:inbound --wait 5m
  ;;

# Undeploy all
'undeploy-all')
  ${KUMORICTL_CMD} unlink $DEPLOYNAME:service $INBOUNDNAME:inbound --wait 5m
  ${KUMORICTL_CMD} unregister deployment $DEPLOYNAME --wait 5m
  ${KUMORICTL_CMD} unregister deployment $INBOUNDNAME --wait 5m
  ${KUMORICTL_CMD} unregister domain $DOMAIN
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac
