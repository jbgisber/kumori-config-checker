#!/bin/bash

CLUSTERNAME="..."

#KUMORICTL_CMD="kam ctl"
KUMORICTL_CMD="kumorictl"

SERVICENAME="cfgcheckerdep"
INBOUNDNAME="cfgcheckerinb"
REFERENCEDOMAIN="test.kumori.cloud"
SERVICEURL="cfgchecker-${CLUSTERNAME}.${REFERENCEDOMAIN}"
ROLEHASH="466dc168"

GETALL=$( ${KUMORICTL_CMD} get deployments | grep -i ${SERVICENAME} )

if [[ ${GETALL} != "" ]]
then
  echo "Removing existing ${SERVICENAME}"
  ${KUMORICTL_CMD} undeploy ${SERVICENAME} --force --wait 5m
fi

echo "Deploying ${SERVICENAME}"
${KUMORICTL_CMD} register deployment ${SERVICENAME} -y -d manifests/deployments/hello1;
${KUMORICTL_CMD} link ${SERVICENAME}:service ${INBOUNDNAME}:inbound

CHECKITER=10
echo "Number of checks: ${CHECKITER}"
UPDITER=10
echo "Number of updates per check: ${UPDITER}"

X=1
Y=3
DIFF=$((Y-X+1))  
LAST=1
RANDOM=$$

for i in $(seq 1 1 $CHECKITER)
do
  echo ""
  echo ""
  echo "-------------------"
  echo "Starting iteration ${i}. Updating ${SERVICENAME} ${UPDITER} times"
  echo "-------------------"
  echo ""
  j=0
  while [ ${j} -lt ${UPDITER} ]
  do
    R=$(($(($RANDOM%$DIFF))+X))
    if [ $LAST -ne $R ]
    then
      NEWDEP="hello$R"
      echo ""
      j=$((j+1))
      if [ ${j} -eq ${UPDITER} ]
      then
        echo "(${j}) Updating to $NEWDEP (with wait)"
        ${KUMORICTL_CMD} update deployment ${SERVICENAME} -y -d manifests/deployments/$NEWDEP --wait 5m
      else
        echo "(${j}) Updating to $NEWDEP (without wait)"
        ${KUMORICTL_CMD} update deployment ${SERVICENAME} -y -d manifests/deployments/$NEWDEP
      fi
      LAST=$R
    fi
  done

  echo "Calculating deployment id"
  DEPLOYMENT=$( curl -s https://${SERVICEURL}/deployment )
  echo "Deployment id: ${DEPLOYMENT}"

  CONFIGMAP="${DEPLOYMENT}-${ROLEHASH}-configmap"
  echo "Getting ConfigMap ${CONFIGMAP}"
  REALCONFIG=$( kubectl get configmap -n kumori ${CONFIGMAP} -o json | jq -r .binaryData.kumoriconfig | base64 -d | jq --sort-keys . )

  date
  beforeSeconds=$SECONDS
  echo -ne "Waiting for ConfigMap and /kumori/config.json to be equal\r"

  INSTANCECONFIG=$( curl -s https://${SERVICEURL}/data | jq --sort-keys  . )
  MYDIFF=$( diff <( echo ${REALCONFIG} ) <( echo ${INSTANCECONFIG} ) )
  while [[ ${MYDIFF} != "" ]]
  do
    elapsed=$(( SECONDS - beforeSeconds ))
    echo -ne "Waiting for ConfigMap and /kumori/config.json to be equal. Elapsed time: ${elapsed}s\r"
    sleep 5
    INSTANCECONFIG=$( curl -s https://${SERVICEURL}/data | jq --sort-keys  . )
    MYDIFF=$( diff <( echo ${REALCONFIG} ) <( echo ${INSTANCECONFIG} ) )
  done
  elapsed=$(( SECONDS - beforeSeconds ))
  echo -ne "Waiting for ConfigMap and /kumori/config.json to be equal. Elapsed time: ${elapsed}s\r"
  echo ""
  date
  echo "/kumori/config.json up to date"

  echo "Iteration ${i} finished"
done
