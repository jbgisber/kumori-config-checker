package deployment

import (
  s ".../services/hello1:service"
)

#Deployment: {
  name: "deployments/hello1"
  artifact: s.#Artifact
  config: {
    parameter: {}
    resource: {}
    scale: detail: {
      cfggetter: hsize: 1
      hello1: hsize: 1
    }
    resilience: 0
  }
}
