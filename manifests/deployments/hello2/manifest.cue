package deployment

import (
  s ".../services/hello2:service"
)

#Deployment: {
  name: "deployments/hello2"
  artifact: s.#Artifact
  config: {
    parameter: {}
    resource: {}
    scale: detail: {
      cfggetter: hsize: 1
      hello1: hsize: 1
      hello2: hsize: 1
    }
    resilience: 0
  }
}
