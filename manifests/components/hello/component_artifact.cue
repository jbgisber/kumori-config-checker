
package component

#Artifact: {
  ref: name:  "component/hello"
  description: {
    srv: {
      server: {
        restapi: { protocol: "http", port: 8080 }
      }
    }
    config: {
      parameter: {}
      resource: {}
    }

    size: {
      bandwidth: {
        size: 10
        unit: "M"
      }
    }

    probe: mainserver: {
      liveness: {
        protocol: http : {
          port: srv.server.restapi.port
          path: "/health"
        }
        startupGraceWindow: {
          unit: "ms",
          duration: 20000,
          probe: true
        }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : {
          port: srv.server.restapi.port
          path: "/health"
        }
        frequency: "medium"
        timeout: 30000 // msec
      }
    }

    code: mainserver: {
      name: "mainserver"
      image: {
        hub: {
          name: "registry.gitlab.com"
          secret: ""
        }
        tag: "kumori-systems/community/image-registry/examples/hello-world-frontend:v1.0.3"
      }
      mapping: {
        env: {
          HTTP_SERVER_PORT_ENV: value: "\(srv.server.restapi.port)"
        }
      }

      size: {
        memory: { size: 100, unit: "M" }
        mincpu: 100
        cpu: { size: 200, unit: "m" }
      }
    }
  }
}
