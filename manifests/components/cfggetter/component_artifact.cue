
package component

#Artifact: {
  ref: name:  "component/cfggetter"
  description: {
    srv: {
      server: {
        restapi: { protocol: "http", port: 8080 }
      }
      client: {
        restcli: { protocol: "http" }
      }
    }
    config: {
      parameter: {}
      resource: {}
    }

    size: {
      bandwidth: {
        size: 10
        unit: "M"
      }
    }

    probe: mainserver: {
      liveness: {
        protocol: http : {
          port: srv.server.restapi.port
          path: "/health"
        }
        startupGraceWindow: {
          unit: "ms",
          duration: 20000,
          probe: true
        }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : {
          port: srv.server.restapi.port
          path: "/health"
        }
        frequency: "medium"
        timeout: 30000 // msec
      }
    }

    code: mainserver: {
      name: "mainserver"
      image: {
        hub: {
          name: "registry.gitlab.com"
          secret: ""
        }        
        tag: "jbgisber/kumori-config-checker:latest"
      }
      mapping: {
        env: {
          SERVER_PORT: value: "\(srv.server.restapi.port)"
        }
      }

      size: {
        memory: { size: 100, unit: "M" }
        mincpu: 100
        cpu: { size: 200, unit: "m" }
      }
    }

  }
}
