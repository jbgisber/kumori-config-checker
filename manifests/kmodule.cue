package kmodule

{
	domain: "kumori.examples"
	module: "cfgchecker"
	version: [
		1,
		0,
		0,
	]
	cue: "v0.4.2"
	spec: [
		1,
		0,
	]
	dependencies: "kumori.systems/kumori": {
		query:  "1.0.6"
		target: "kumori.systems/kumori/@1.0.6"
	}
	sums: "kumori.systems/kumori/@1.0.6": "2GLIVPzXVnSRizKaxXFua9QjiyBgwbBdCuHmyyMGf30="
	artifacts: {
		"components/hello":     "component"
		"components/cfggetter": "component"
		"services/hello1":      "service"
		"services/hello2":      "service"
		"services/hello3":      "service"
	}
}
