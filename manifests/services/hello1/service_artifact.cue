
package service

import (
  h ".../components/hello:component"
  g ".../components/cfggetter:component"
)

#Artifact: {
  ref: name:  "services/hello1"
  description: {

    srv: {
      server: {
        service: { protocol: "http", port: 80 }
      }
    }

    config: {
      parameter: {}
      resource: {}
    }

    role: {
      cfggetter: {
        artifact: g.#Artifact
        config: {
          resilience: description.config.resilience
        }
      }
      hello1: {
        artifact: h.#Artifact
        config: {
          resilience: description.config.resilience
        }
      }
    }

    vset: hello: {
      srv: {
        server: restapi: _
      }
      roles: {
        hello1: meta: { hello: 1 }
      }
    }

    connect: {
      serviceconnector: {
        as: "lb"
			  from: self: "service"
        to: cfggetter: "restapi": _
      }
      lbconnector: {
        as: "lb"
        from: cfggetter: "restcli"
        to: hello: "restapi": _
      }
    }
  }
}
