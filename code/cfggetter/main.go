package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {

	fmt.Println("Starting")

	// Reads the $CONFIG_PATH environment variable. If not set,
	// the current folder is taken by default
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		fmt.Println("CONFIG_PATH environment variable not found. Using /kumori/config.json")
		configPath = "/kumori/config.json"
	}
	fmt.Printf("Config path: %s\n", configPath)

	// Reads the $SERVER_PORT environment variable. If not set,
	// 8080 is taken by default
	serverPort := os.Getenv("SERVER_PORT")
	if serverPort == "" {
		fmt.Println("SERVER_PORT environment variable not found. Using 8080")
		serverPort = "8080"
	}
	fmt.Printf("Server port: %s\n", serverPort)

	// Reads the $KUMORI_DEPLOYMENT environment variable. If not set, fails
	deploymentId := os.Getenv("KUMORI_DEPLOYMENT")
	if deploymentId == "" {
		fmt.Println("KUMORI_DEPLOYMENT environment variable not found. Aborting...")
		os.Exit(1)
	}
	fmt.Printf("Deployment ID: %s\n", deploymentId)

	// Run server
	http.HandleFunc("/data", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Processing data request")

		// Read file from volumepath
		data, err := os.ReadFile(configPath)
		if err != nil {
			fmt.Printf("Error getting config file: %v\n", err)
			fmt.Fprintf(w, "Error getting config file: %v", err)
		} else {
			fmt.Fprintf(w, "%s", string(data))
		}
	})

	http.HandleFunc("/deployment", func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("Processing deployment request. Deployment: %s\n", deploymentId)
		fmt.Fprint(w, deploymentId)
	})

	http.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Processing health")
		fmt.Fprint(w, "ok")
	})

	fmt.Printf("Listening in port %s\n", serverPort)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", serverPort), nil))
}
